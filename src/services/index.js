/*
   followers service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const kafka = require('./kafka.js')

function kafkaConnect(consumer) {
    return new Promise(resolve => {
        consumer.connect()
    })
}

async function startServices(kafkaBrokers, kafkaClientId, kafkaGroupId,
    kafkaTopics) {
    /* Start kafka consumer */
    var consumer = new kafka.Consumer(kafkaBrokers, kafkaClientId, kafkaGroupId)
    try {
        await kafkaConnect(consumer)
    } catch(err) {
        console.log(`error can't connect to kafka server: ${err.message}`)
        process.exit(1)
    }
    console.log('connected consumer to kafka server')
    console.log(`subscribing kafka consumer to topic: ${kafkaTopics}`)
    consumer.run()
}

module.exports = {
    startServices,
}
