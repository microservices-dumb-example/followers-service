/*
   followers service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const dotenv = require('dotenv')

/* Required environment variables */
const requiredEnvVars = [
    'KAFKA_BROKER_HOST',
    'KAFKA_BROKER_PORT',
    'KAFKA_CLIENT_ID',
    'KAFKA_GROUP_ID',
    'KAFKA_TOPIC_USERS',
]

/* Load dotenv config */
dotenv.config()

/* Check required environment variables */
for (i = 0; i < requiredEnvVars.length; i++) {
    if (!(requiredEnvVars[i] in process.env)) {
        console.log(
            `error missing required environment variable ${requiredEnvVars[i]}`
        )
        process.exit(1)
    }
}

/* Kafka listener configuration */
exports.kafkaBrokerHost = process.env.KAFKA_BROKER_HOST
exports.kafkaBrokerPort = process.env.KAFKA_BROKER_PORT
exports.kafkaClientId = process.env.KAFKA_CLIENT_ID
exports.kafkaGroupId = process.env.KAFKA_GROUP_ID
exports.kafkaTopicUsers = process.env.KAFKA_TOPIC_USERS
