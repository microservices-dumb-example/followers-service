/*
   followers service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const config = require('./config')
const services = require('./services')

/* Set kafka config */
var kafkaBrokers = [config.kafkaBrokerHost+':'+config.kafkaBrokerPort]
var kafkaTopics = [
    config.kafkaTopicUsers,
]

console.log('starting service')
services.startServices(kafkaBrokers, config.kafkaClientId, config.kafkaGroupId,
    kafkaTopics)
